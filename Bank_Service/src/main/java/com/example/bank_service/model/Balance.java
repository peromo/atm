package com.example.bank_service.model;


import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "Balance")
@NamedQueries({
        @NamedQuery(name = "Balance.findByAccount_AccNum", query = "select b from Balance b where b.account.accNum = :accNum"),
        @NamedQuery(name = "Balance.updateByBalance", query = "update Balance b set b.balance = :balance + b.balance " +
                "where b.balance = :balance", lockMode = LockModeType.PESSIMISTIC_WRITE)
})
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Balance {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", nullable = false)
    private Long id;


    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "accountId", referencedColumnName = "id")
    private Account account;


    private BigDecimal balance;
}
