package com.example.bank_service.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/bank/user")
public class UsrController {

    @GetMapping
    ResponseEntity<String> getAllSales() {
        return new ResponseEntity<>("Hello World",HttpStatus.OK);
    }
}
