package com.example.bank_service.apierror;


import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.util.Map;

public class FormatException extends RuntimeException {

    public FormatException(Class clazz,  String message) {
        super(FormatException.generateMessage(clazz.getSimpleName(),message));
    }

    private static String generateMessage(String entity,String message) {
        return StringUtils.capitalize(entity) +
                " Format is not Valid "
                +message;
    }

}
