package com.example.bank_service.apierror;


import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.util.Map;

public class ValidationFoundException extends RuntimeException {

    public ValidationFoundException(Class clazz,Map<Integer, BigDecimal> searchParamsMap,String message) {
        super(ValidationFoundException.generateMessage(clazz.getSimpleName(),  searchParamsMap,message));
    }

    private static String generateMessage(String entity, Map<Integer, BigDecimal> searchParamsMap,String message) {
        return StringUtils.capitalize(entity) +
                " Parameter is not Valid " +
                (searchParamsMap==null ? "" : searchParamsMap.toString())
                + " " +message;
    }

}
