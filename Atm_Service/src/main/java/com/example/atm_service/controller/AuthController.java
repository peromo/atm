package com.example.atm_service.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/auth")
public class AuthController {

    @GetMapping
    ResponseEntity<String> getAllSales() {
        return new ResponseEntity<>("Hello AtmService(AuthController)", HttpStatus.OK);
    }
}
