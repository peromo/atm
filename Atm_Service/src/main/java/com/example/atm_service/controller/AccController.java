package com.example.atm_service.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
@RequestMapping("/api/accounting")
public class AccController {

    @Autowired
    RestTemplate restTemplate;

    @GetMapping
    ResponseEntity<String> getAllSales() {
        return new ResponseEntity<>("Hello AtmService(AccController)",HttpStatus.OK);
    }


    @GetMapping("bank")
    ResponseEntity<String> getBankHelloService() {

        return new ResponseEntity<String>("",HttpStatus.OK);
    }

}
